package com.souvc.wordpress4j.domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 文章评论信息表
 */
@Entity
@Table(name = "wp_comments")
public class Comments implements Serializable {

    private static final long serialVersionUID = 7246382281551532684L;

    /**
     * 每个评论的唯一ID号，是一个bigint(20)值。带有附加属性auto_increment
     */
    @Id
    @Column(name = "comment_ID",length = 20,nullable = false)
    @GeneratedValue
    private long commentId;

    /**
     * 每个评论对应的文章的ID号，int(11)值，等同于wp_posts.ID
     */
    @Basic
    @Column(name = "comment_post_ID",length = 11)
    private long commentPostId;

    /**
     * 每个评论的评论者名称，tinytext值
     */
    @Basic
    @Column(name = "comment_author")
    private String commentAuthor;

    /**
     * 每个评论的评论者电邮地址
     */
    @Basic
    @Column(name = "comment_author_email",length = 100)
    private String commentAuthorEmail;

    /**
     * 每个评论的评论者网址
     */
    @Basic
    @Column(name = "comment_author_url",length = 200)
    private String commentAuthorUrl;

    /**
     * 每个评论的评论者的IP地址，varchar(100)值
     */
    @Basic
    @Column(name = "comment_author_IP",length = 100)
    private String commentAuthorIp;

    /**
     * 每个评论发表的时间，datetime值(是加上时区偏移量后的值
     */
    @Basic
    @Column(name = "comment_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date commentDate;

    /**
     * 每个评论发表的时间，datetime值(是标准的格林尼治时间
     */
    @Basic
    @Column(name = "comment_date_gmt")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date commentDateGmt;

    /**
     * 每个评论的具体内容，text值。
     */
    @Basic
    @Column(name = "comment_content")
    private String commentContent;

    /**
     * 默认为0。
     */
    @Basic
    @Column(name = "comment_karma",length = 11)
    private int commentKarma;

    /**
     * 每个评论的当前状态，为一个枚举值enum('0','1','spam')，0为等待审核，1为允许发布，spam为垃圾评论。默认值为1
     */
    @Basic
    @Column(name = "comment_approved")
    private String commentApproved;

    /**
     * 每个评论的评论者的客户端信息，主要包括其浏览器和操作系统的类型、版本等资料。
     */
    @Basic
    @Column(name = "comment_agent")
    private String commentAgent;

    /**
     * 评论的类型
     */
    @Basic
    @Column(name = "comment_type",length = 20)
    private String commentType;

    /**
     * 某一评论的上级评论，对应wp_comment.ID，默认为0，即无上级评论
     */
    @Basic
    @Column(name = "comment_parent",length = 11)
    private long commentParent;

    /**
     * 某一评论对应的用户ID，只有当用户注册后才会生成，int(11)值，对应wp_users.ID。未注册的用户，即外部评论者，这个ID的值为0
     */
    @Basic
    @Column(name = "user_id",length = 11)
    private long userId;

    //get，set方法
    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public long getCommentPostId() {
        return commentPostId;
    }

    public void setCommentPostId(long commentPostId) {
        this.commentPostId = commentPostId;
    }

    public String getCommentAuthor() {
        return commentAuthor;
    }

    public void setCommentAuthor(String commentAuthor) {
        this.commentAuthor = commentAuthor;
    }

    public String getCommentAuthorEmail() {
        return commentAuthorEmail;
    }

    public void setCommentAuthorEmail(String commentAuthorEmail) {
        this.commentAuthorEmail = commentAuthorEmail;
    }

    public String getCommentAuthorUrl() {
        return commentAuthorUrl;
    }

    public void setCommentAuthorUrl(String commentAuthorUrl) {
        this.commentAuthorUrl = commentAuthorUrl;
    }

    public String getCommentAuthorIp() {
        return commentAuthorIp;
    }

    public void setCommentAuthorIp(String commentAuthorIp) {
        this.commentAuthorIp = commentAuthorIp;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public Date getCommentDateGmt() {
        return commentDateGmt;
    }

    public void setCommentDateGmt(Date commentDateGmt) {
        this.commentDateGmt = commentDateGmt;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public int getCommentKarma() {
        return commentKarma;
    }

    public void setCommentKarma(int commentKarma) {
        this.commentKarma = commentKarma;
    }

    public String getCommentApproved() {
        return commentApproved;
    }

    public void setCommentApproved(String commentApproved) {
        this.commentApproved = commentApproved;
    }

    public String getCommentAgent() {
        return commentAgent;
    }

    public void setCommentAgent(String commentAgent) {
        this.commentAgent = commentAgent;
    }

    public String getCommentType() {
        return commentType;
    }

    public void setCommentType(String commentType) {
        this.commentType = commentType;
    }

    public long getCommentParent() {
        return commentParent;
    }

    public void setCommentParent(long commentParent) {
        this.commentParent = commentParent;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
