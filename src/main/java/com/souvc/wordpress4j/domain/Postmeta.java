package com.souvc.wordpress4j.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 *  用于保存文章的元信息(meta)的表。文章额外数据表，
 *  例如文章浏览次数，文章的自定义字段等都存储在这里。
 */
@Entity
@Table(name = "wp_postmeta")
public class Postmeta implements Serializable{

    /**
     * 元信息ID，bigint(20)值，附加属性为auto_increment。
     */
    @Id
    @Column(name = "meta_id",nullable = false)
    @GeneratedValue
    private long metaId;

    /**
     * 文章ID，bigint(20)值，相当于wp_posts.ID
     */
    @Basic
    @Column(name = "post_id")
    private long postId;

    /**
     * 元信息的关键字，varchar(255)值。
     * meta_key就对应名为”key”的下拉列表中的项，而值由用户自己填上(某些时候，wp也会自动加入，如文 章中有的音频媒体)。
     */
    @Basic
    @Column(name = "meta_key")
    private String metaKey;

    /**
     *  元信息的值，text值
     */
    @Basic
    @Column(name = "meta_value")
    private String metaValue;


    public long getMetaId() {
        return metaId;
    }

    public void setMetaId(long metaId) {
        this.metaId = metaId;
    }


    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }


    public String getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }


    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }

}
