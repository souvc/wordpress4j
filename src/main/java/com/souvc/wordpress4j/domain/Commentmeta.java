package com.souvc.wordpress4j.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 文章评论额外信息表。
 */
@Entity
@Table(name = "wp_commentmeta")
public class Commentmeta implements Serializable {

    private static final long serialVersionUID = -3877718503332391909L;

    @Id
    @Column(name = "meta_id",nullable = false)
    @GeneratedValue
    private long metaId;

    @Basic
    @Column(name = "comment_id")
    private long commentId;

    @Basic
    @Column(name = "meta_key")
    private String metaKey;

    @Basic
    @Column(name = "meta_value")
    private String metaValue;

    public long getMetaId() {
        return metaId;
    }

    public void setMetaId(long metaId) {
        this.metaId = metaId;
    }


    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }


    public String getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }


    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }
}
