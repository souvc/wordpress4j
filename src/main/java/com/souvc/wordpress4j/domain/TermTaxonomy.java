package com.souvc.wordpress4j.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 分类信息表，区分wp_terms信息的分类类型，
 * 有category、link_category和tag三种分类类型。
 */
@Entity
@Table(name = "wp_term_taxonomy")
public class TermTaxonomy implements Serializable{
    @Id
    @Column(name = "term_taxonomy_id")
    @GeneratedValue
    private long termTaxonomyId;

    @Basic
    @Column(name = "term_id")
    private long termId;

    @Basic
    @Column(name = "taxonomy")
    private String taxonomy;

    @Basic
    @Column(name = "description")
    private String description;

    @Basic
    @Column(name = "parent")
    private long parent;

    @Basic
    @Column(name = "count")
    private long count;

    public long getTermId() {
        return termId;
    }

    public void setTermId(long termId) {
        this.termId = termId;
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public long getParent() {
        return parent;
    }

    public void setParent(long parent) {
        this.parent = parent;
    }


    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

}
