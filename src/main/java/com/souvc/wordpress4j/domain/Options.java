package com.souvc.wordpress4j.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 用于保存Wordpress相关设置、参数的表。
 * 基本配置信息表，通常通过get_option来操作，
 * 该表通常作为插件存储数据的一个地方。是用来存储 WordPress 中所有全局选项的数据表。
 */
@Entity
@Table(name = "wp_options")
public class Options implements Serializable{

    /**
     *  选项的ID，bigint(20)值，附加auto_increment属性。
     */
    @Id
    @Column(name = "option_id",nullable = false)
    @GeneratedValue
    private long optionId;

    /**
     * 选项名称，varchar(64)值。
     */
    @Basic
    @Column(name = "option_name")
    private String optionName;

    /**
     *  选项的值，longtext值
     */
    @Basic
    @Column(name = "option_value")
    private String optionValue;

    /**
     * autoload – 选项是否每次都被自动加载，枚举enum('yes','no')值，默认为yes。
     */
    @Basic
    @Column(name = "autoload")
    private String autoload;


    public long getOptionId() {
        return optionId;
    }

    public void setOptionId(long optionId) {
        this.optionId = optionId;
    }


    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }


    public String getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }


    public String getAutoload() {
        return autoload;
    }

    public void setAutoload(String autoload) {
        this.autoload = autoload;
    }

}
