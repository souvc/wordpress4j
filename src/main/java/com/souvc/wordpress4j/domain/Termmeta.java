package com.souvc.wordpress4j.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 分类与文章信息表（wp_posts）、
 * 链接表(wp_links)的关联表
 */
@Entity
@Table(name = "wp_termmeta")
public class Termmeta implements Serializable{

    @Id
    @Column(name = "meta_id",nullable = false)
    @GeneratedValue
    private long metaId;

    @Basic
    @Column(name = "term_id")
    private long termId;

    @Basic
    @Column(name = "meta_key")
    private String metaKey;

    @Basic
    @Column(name = "meta_value")
    private String metaValue;


    public long getMetaId() {
        return metaId;
    }

    public void setMetaId(long metaId) {
        this.metaId = metaId;
    }

    public long getTermId() {
        return termId;
    }

    public void setTermId(long termId) {
        this.termId = termId;
    }

    public String getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }
}
