package com.souvc.wordpress4j.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 文章分类、链接分类、标签的信息表
 */
@Entity
@Table(name = "wp_terms")
public class Terms implements Serializable{
    @Id
    @Column(name = "term_id")
    @GeneratedValue
    private long termId;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "slug")
    private String slug;

    @Basic
    @Column(name = "term_group")
    private long termGroup;

    public long getTermId() {
        return termId;
    }

    public void setTermId(long termId) {
        this.termId = termId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public long getTermGroup() {
        return termGroup;
    }

    public void setTermGroup(long termGroup) {
        this.termGroup = termGroup;
    }
}
