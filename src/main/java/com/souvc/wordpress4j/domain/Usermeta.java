package com.souvc.wordpress4j.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 用于保存用户元信息(meta)的表。
 */
@Entity
@Table(name = "wp_usermeta")
public class Usermeta implements Serializable{

    /**
     * 元信息ID，bigint(20)值，附加属性auto_increment。
     */
    @Id
    @Column(name = "umeta_id")
    @GeneratedValue
    private long umetaId;

    /**
     * 元信息对应的用户ID，bigint(20)值，相当于wp_users.ID。
     */
    @Basic
    @Column(name = "user_id")
    private long userId;

    /**
     * 元信息关键字，varchar(255)值。
     */
    @Basic
    @Column(name = "meta_key")
    private String metaKey;

    /**
     * 元信息的详细值，longtext值
     */
    @Basic
    @Column(name = "meta_value")
    private String metaValue;

    public long getUmetaId() {
        return umetaId;
    }

    public void setUmetaId(long umetaId) {
        this.umetaId = umetaId;
    }


    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }


    public String getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }


    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }

}
