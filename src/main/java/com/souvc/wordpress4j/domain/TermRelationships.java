package com.souvc.wordpress4j.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 分类与文章信息表（wp_posts）、链接表(wp_links)的关联表。
 */
@Entity
@Table(name = "wp_term_relationships")
public class TermRelationships implements Serializable{

    @Id
    @Column(name = "object_id")
    @GeneratedValue
    private long objectId;

    @Id
    @Column(name = "term_taxonomy_id")
    private long termTaxonomyId;

    @Basic
    @Column(name = "term_order")
    private int termOrder;

    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    public long getTermTaxonomyId() {
        return termTaxonomyId;
    }

    public void setTermTaxonomyId(long termTaxonomyId) {
        this.termTaxonomyId = termTaxonomyId;
    }

    public int getTermOrder() {
        return termOrder;
    }

    public void setTermOrder(int termOrder) {
        this.termOrder = termOrder;
    }
}
