package com.souvc.wordpress4j.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 用于保存Wordpress使用者的相关信息的表。
 */
@Entity
@Table(name = "wp_users")
public class Users implements Serializable{

    private static final long serialVersionUID = -3005880895919713708L;

    /**
     * 用户唯一ID,bigint(20)值，带附加属性auto_increment
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue
    private long id;

    /**
     *  用户的注册名称，varchar(60)值
     */
    @Basic
    @Column(name = "user_login")
    private String userLogin;

    /**
     * 用户密码，varchar(64)值，这是经过加密的结果。好象用的是不可逆的MD5算法
     */
    @Basic
    @Column(name = "user_pass")
    private String userPass;

    /**
     * 用户昵称，varchar(50)值
     */
    @Basic
    @Column(name = "user_nicename")
    private String userNicename;

    /**
     * 用户电邮地址，varchar(100)值
     */
    @Basic
    @Column(name = "user_email")
    private String userEmail;

    /**
     * 用户网址，varchar(100)值
     */
    @Basic
    @Column(name = "user_url")
    private String userUrl;

    /**
     * 用户注册时间，datetime值
     */
    @Basic
    @Column(name = "user_registered")
    private Timestamp userRegistered;

    /**
     * 用户激活码。varchar(60)值
     */
    @Basic
    @Column(name = "user_activation_key")
    private String userActivationKey;

    /**
     * 用户状态，int(11)值，默认为0
     */
    @Basic
    @Column(name = "user_status")
    private int userStatus;

    /**
     * 来前台显示出来的用户名字，varchar(250)值
     */
    @Basic
    @Column(name = "display_name")
    private String displayName;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }


    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }


    public String getUserNicename() {
        return userNicename;
    }

    public void setUserNicename(String userNicename) {
        this.userNicename = userNicename;
    }


    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }


    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }


    public Timestamp getUserRegistered() {
        return userRegistered;
    }

    public void setUserRegistered(Timestamp userRegistered) {
        this.userRegistered = userRegistered;
    }


    public String getUserActivationKey() {
        return userActivationKey;
    }

    public void setUserActivationKey(String userActivationKey) {
        this.userActivationKey = userActivationKey;
    }


    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

}
