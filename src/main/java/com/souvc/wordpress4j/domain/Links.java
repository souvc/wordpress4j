package com.souvc.wordpress4j.domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 用于保存用户输入到Wordpress中的链接的表
 */
@Entity
@Table(name = "wp_links")
public class Links implements Serializable {

    /**
     * 每个链接的唯一ID号，bigint(20)值，附加属性为auto_increment。
     */
    @Id
    @Column(name = "link_id",nullable = false)
    @GeneratedValue
    private long linkId;

    /**
     * 每个链接的URL地址，varchar(255)值，形式为http://开头的地址
     */
    @Basic
    @Column(name = "link_url")
    private String linkUrl;

    /**
     * 单个链接的名字，varchar(255)值。
     */
    @Basic
    @Column(name = "link_name")
    private String linkName;

    /**
     * 链接可以被定义为使用图片链接，这个字段用于保存该图片的地址，为varchar(255)值。
     */
    @Basic
    @Column(name = "link_image")
    private String linkImage;

    /**
     *  链接打开的方式，有三种，_blank为以新窗口打开，_top为就在本窗口中打开并在最上一级，
     *  none为不选择，会在本窗口中打开。这个字段是varchar(25)值。
     */
    @Basic
    @Column(name = "link_target",length = 25)
    private String linkTarget;

    /**
     *  链接的说明文字。用户可以选择显示在链接下方还是显示在title属性中。varchar(255)值。
     */
    @Basic
    @Column(name = "link_description")
    private String linkDescription;

    /**
     * 该链接是否可以，枚举enum(’Y’,’N’)值，默认为Y，即可见。
     */
    @Basic
    @Column(name = "link_visible")
    private String linkVisible;

    /**
     * 某个链接的创建人，为一int(11)值，默认是1。(应该对应的就是wp_users.ID)
     */
    @Basic
    @Column(name = "link_owner")
    private long linkOwner;

    /**
     *  链接的等级，int(11)值。默认为0。
     */
    @Basic
    @Column(name = "link_rating")
    private int linkRating;

    /**
     * 链接被定义、修改的时间，datetime值。
     */
    @Basic
    @Column(name = "link_updated")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date linkUpdated;

    /**
     * 链接与定义者的关系，由XFN Creator设置，varchar(255)值。
     */
    @Basic
    @Column(name = "link_rel")
    private String linkRel;

    /**
     *  链接的详细说明，mediumtext值。
     */
    @Basic
    @Column(name = "link_notes")
    private String linkNotes;

    /**
     * 该链接的RSS地址，varchar(255)值。
     */
    @Basic
    @Column(name = "link_rss")
    private String linkRss;

    //get，set方法
    public long getLinkId() {
        return linkId;
    }

    public void setLinkId(long linkId) {
        this.linkId = linkId;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public String getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }

    public String getLinkTarget() {
        return linkTarget;
    }

    public void setLinkTarget(String linkTarget) {
        this.linkTarget = linkTarget;
    }

    public String getLinkDescription() {
        return linkDescription;
    }

    public void setLinkDescription(String linkDescription) {
        this.linkDescription = linkDescription;
    }

    public String getLinkVisible() {
        return linkVisible;
    }

    public void setLinkVisible(String linkVisible) {
        this.linkVisible = linkVisible;
    }

    public long getLinkOwner() {
        return linkOwner;
    }

    public void setLinkOwner(long linkOwner) {
        this.linkOwner = linkOwner;
    }

    public int getLinkRating() {
        return linkRating;
    }

    public void setLinkRating(int linkRating) {
        this.linkRating = linkRating;
    }

    public Date getLinkUpdated() {
        return linkUpdated;
    }

    public void setLinkUpdated(Date linkUpdated) {
        this.linkUpdated = linkUpdated;
    }

    public String getLinkRel() {
        return linkRel;
    }

    public void setLinkRel(String linkRel) {
        this.linkRel = linkRel;
    }

    public String getLinkNotes() {
        return linkNotes;
    }

    public void setLinkNotes(String linkNotes) {
        this.linkNotes = linkNotes;
    }

    public String getLinkRss() {
        return linkRss;
    }

    public void setLinkRss(String linkRss) {
        this.linkRss = linkRss;
    }
}
