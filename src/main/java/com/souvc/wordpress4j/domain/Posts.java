package com.souvc.wordpress4j.domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 用于保存你所有的文章(posts)的相关信息的表。文章信息表，
 * 包括了日志、附件、页面等等信息，是WordPress最重要的一个数据表。
 */
@Entity
@Table(name = "wp_posts")
public class Posts implements Serializable{

    /**
     * 每篇文章的唯一ID，bigint(20)值，附加属性auto_increment
     */
    @Id
    @Column(name = "ID",nullable = false)
    @GeneratedValue
    private long id;

    /**
     * 每篇文章的作者的编号，int(4)值，应该对应的是wp_users.ID
     */
    @Basic
    @Column(name = "post_author")
    private long postAuthor;

    /**
     * 每篇文章发表的时间，datetime值。它是GMT时间加上时区偏移量的结果。
     */
    @Basic
    @Column(name = "post_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date postDate;

    /**
     * 每篇文章发表时的GMT(格林威治)时间，datetime值。
     */
    @Basic
    @Column(name = "post_date_gmt")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date postDateGmt;


    /**
     *  每篇文章的具体内容，longtext值。你在后台文章编辑页面中写入的所有内容都放在这里
     */
    @Basic
    @Column(name = "post_content")
    private String postContent;

    /**
     *  文章的标题，text值
     */
    @Basic
    @Column(name = "post_title")
    private String postTitle;

    /**
     * 文章摘要，text值
     */
    @Basic
    @Column(name = "post_excerpt")
    private String postExcerpt;

    /**
     * 文章当前的状态，枚举enum(’publish’,’draft’,’private’,’static’,’object’)值，
     * publish为已 发表，draft为草稿，private为私人内容(不会被公开) ，static(不详)，object(不详)。
     * 默认为publish。
     */
    @Basic
    @Column(name = "post_status")
    private String postStatus;

    /**
     * 评论设置的状态，也是枚举enum(’open’,’closed’,’registered_only’)值，open为允许评论，closed为不允 许评论，
     * registered_only为只有注册用户方可评论。默认为open，即人人都可以评论。
     */
    @Basic
    @Column(name = "comment_status")
    private String commentStatus;

    /**
     * ping状态，枚举enum(’open’,’closed’)值，open指打开pingback功能，closed为关闭。默认值是open。
     */
    @Basic
    @Column(name = "ping_status")
    private String pingStatus;

    /**
     * 文章密码，varchar(20)值。文章编辑才可为文章设定一个密码，凭这个密码才能对文章进行重新强加或修改。
     */
    @Basic
    @Column(name = "post_password")
    private String postPassword;

    /**
     * 文章名，varchar(200)值。这通常是用在生成permalink时，标识某篇文章的一段文本或数字，也即post slug。
     */
    @Basic
    @Column(name = "post_name")
    private String postName;

    /**
     *  强制该文章去ping某个URI。text值。
     */
    @Basic
    @Column(name = "to_ping")
    private String toPing;

    /**
     *  该文章被pingback的历史记录，text值，为一个个的URI。
     */
    @Basic
    @Column(name = "pinged")
    private String pinged;

    /**
     * 文章最后修改的时间，datetime值，它是GMT时间加上时区偏移量的结果
     */
    @Basic
    @Column(name = "post_modified")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date postModified;

    /**
     *  文章最后修改的GMT时间，datetime值。
     */
    @Basic
    @Column(name = "post_modified_gmt")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date postModifiedGmt;

    @Basic
    @Column(name = "post_content_filtered")
    private String postContentFiltered;


    /**
     *  文章的上级文章的ID，int(11)值，对应的是wp_posts.ID。默认为0，即没有上级文章。
     */
    @Basic
    @Column(name = "post_parent")
    private long postParent;

    /**
     * 这是每篇文章的一个地址，varchar(255)值。默认是这样的形式: http://your.blog.site/?p=1，
     * 如果你形成permalink功能，则通常会是: 你的Wordpress站点地址+文章名。
     */
    @Basic
    @Column(name = "guid")
    private String guid;

    /**
     *
     */
    @Basic
    @Column(name = "menu_order")
    private int menuOrder;

    /**
     *  文章类型
     */
    @Basic
    @Column(name = "post_type")
    private String postType;

    @Basic
    @Column(name = "post_mime_type")
    private String postMimeType;

    /**
     * 评论计数
     */
    @Basic
    @Column(name = "comment_count")
    private long commentCount;

    //get、set方法
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPostAuthor() {
        return postAuthor;
    }

    public void setPostAuthor(long postAuthor) {
        this.postAuthor = postAuthor;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public Date getPostDateGmt() {
        return postDateGmt;
    }

    public void setPostDateGmt(Date postDateGmt) {
        this.postDateGmt = postDateGmt;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostExcerpt() {
        return postExcerpt;
    }

    public void setPostExcerpt(String postExcerpt) {
        this.postExcerpt = postExcerpt;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public String getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getPingStatus() {
        return pingStatus;
    }

    public void setPingStatus(String pingStatus) {
        this.pingStatus = pingStatus;
    }

    public String getPostPassword() {
        return postPassword;
    }

    public void setPostPassword(String postPassword) {
        this.postPassword = postPassword;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getToPing() {
        return toPing;
    }

    public void setToPing(String toPing) {
        this.toPing = toPing;
    }

    public String getPinged() {
        return pinged;
    }

    public void setPinged(String pinged) {
        this.pinged = pinged;
    }

    public Date getPostModified() {
        return postModified;
    }

    public void setPostModified(Date postModified) {
        this.postModified = postModified;
    }

    public Date getPostModifiedGmt() {
        return postModifiedGmt;
    }

    public void setPostModifiedGmt(Date postModifiedGmt) {
        this.postModifiedGmt = postModifiedGmt;
    }

    public String getPostContentFiltered() {
        return postContentFiltered;
    }

    public void setPostContentFiltered(String postContentFiltered) {
        this.postContentFiltered = postContentFiltered;
    }

    public long getPostParent() {
        return postParent;
    }

    public void setPostParent(long postParent) {
        this.postParent = postParent;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(int menuOrder) {
        this.menuOrder = menuOrder;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getPostMimeType() {
        return postMimeType;
    }

    public void setPostMimeType(String postMimeType) {
        this.postMimeType = postMimeType;
    }

    public long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(long commentCount) {
        this.commentCount = commentCount;
    }
}
