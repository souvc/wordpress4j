package com.souvc.wordpress4j.repository;

import com.souvc.wordpress4j.domain.Users;
import org.springframework.data.domain.*;
import org.springframework.data.repository.*;

public interface UsersRepository extends Repository<Users, Long> {

    Page<Users> findAll(Pageable pageable);

    Users findByUserEmail(String email);
}